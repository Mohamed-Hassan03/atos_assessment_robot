*** Settings ***
Library   SeleniumLibrary
Resource    PO/Atos-Assessment-PO.robot



*** Keywords ***

Navigate to Travels Page
    Atos-Assessment-PO.Navigate to Travels Page

Navigate to Sign Up Page
    Atos-Assessment-PO.Navigate to Sign Up Page

Begin Web Test
    Atos-Assessment-PO.Begin Web Test

End Web Test
    Atos-Assessment-PO.End Web Test

End All Test
    Atos-Assessment-PO.End All Test

Verify success Registration and Login
    Atos-Assessment-PO.Verify success Registration and Login

Verify Password Feild Regex
    Atos-Assessment-PO.Verify Password Feild Regex

Verify First Name Field Regex
    Atos-Assessment-PO.Verify First Name Field Regex

Verify Last Name Field Regex
    Atos-Assessment-PO.Verify Last Name Field Regex

Verify Email Field Regex
    Atos-Assessment-PO.Verify Email Field Regex

Verify Mobile NUM Field Regex
    Atos-Assessment-PO.Verify Mobile NUM Field Regex