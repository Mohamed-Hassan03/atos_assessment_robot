*** Settings ***
Resource  ../../ObjectRepository/Atos-Assessment-Locators.robot
Resource  ../../CL/Atos-Assessment-CL.robot


*** Variables ***
${CORRECT_FNAME_REGEX}=     Mohamed
${CORRECT_LNAME_REGEX}=     Hassan
${CORRECT_MOBNUM_REGEX}=    012345678
${CORRECT_EMAIL_REGEX}=     mohamed@vodafone.com
${CORRECT_PW_REGEX}=        @bcD12349

${WRONG_PW_REGEX1}=     aBcd@
${WRONG_PW_REGEX2}=     abcdefg@
${WRONG_PW_REGEX3}=     ABCDEFG@
${WRONG_PW_REGEX4}=     ABCDEFg1
${NOT_MATCED_PW}=       @bcD56787

${WRONG_FNAME_REGEX}=    mohamed
${WRONG_LNAME_REGEX}=    hassan
${WRONG_EMAIL_REGEX}=    mohamed
${WRONG_MOB_NUM_REGEX}=    mohamed

*** Keywords ***

Begin Web Test
    Open Browser  about:blank  ${BROWSER}    # New
    Maximize Browser Window                  # New

End Web Test
    Close Browser

End All Test
    Close All Browsers

Navigate to Travels Page
    Go To   ${TRAVEL_PAGE_URL}
    Capture Page Screenshot     ../Results/Atos/HomePage_{index}.png


Navigate to Sign Up Page
    Scroll Element Into View        ${SIGN_UP_DDL}
    Click Element                   ${SIGN_UP_DDL}
    Scroll Element Into View        ${SIGN_UP_ICON}
    Click Element                   ${SIGN_UP_ICON}
    Wait Until Element Is visible   ${FIRST_NAME_FEILD}     10s
    Capture Page Screenshot     ../Results/Atos/SignUpPage_{index}.png

Verify success Registration and Login
    Verify Password Feild Regex
    #Will Enter Valid Inputs For All Field Except PW Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${CORRECT_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${CORRECT_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${CORRECT_MOBNUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep  5s
    Click Element    ${SIGN_UP_DDL}
    Click Element    ${SIGN_UP_ICON}
    Sleep  8s
    Click Element    ${LOGIN_USERNAME}
    Input Text  ${LOGIN_USERNAME}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${LOGIN_PW}
    Input Text  ${LOGIN_PW}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${LOGIN_BTN}
    Wait Until Element Is visible   ${WELCOME_MSG}     10s
    Log To Console      "Successeded registration verified successfully"


Verify Password Feild Regex
    #Will Enter Valid Inputs For All Field Except PW Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${CORRECT_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${CORRECT_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${CORRECT_MOBNUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s

    #Verify PW Not Match Confirm PW
    Sleep   5s
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${WRONG_PW_REGEX4}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${NOT_MATCED_PW}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    ${WELCOME_MSG}
    Run Keyword If    'True'=='${status}'    Fail    "Error: Form Accepted Not Matched Passwords"
        ...     ELSE        Log To Console      "Not Matched PW Regex Verified Correctly"

    #Verify First PW Regex Criteria by Entering PW Without Numbers
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${WRONG_PW_REGEX1}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${WRONG_PW_REGEX1}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    ${WELCOME_MSG}
    Run Keyword If    'True'=='${status}'    Fail    "Error: PW Field Accepted Value Without Numbers"

    #Verify Second PW Regex Criteria by Entering PW Without Capital Character
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${WRONG_PW_REGEX2}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${WRONG_PW_REGEX2}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    ${WELCOME_MSG}
    Run Keyword If    'True'=='${status}'    Fail    "Error: PW Field Accepted Value Without Capital Character"

    #Verify Third PW Regex Criteria by Entering PW Without Small Character
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${WRONG_PW_REGEX3}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${WRONG_PW_REGEX3}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    ${WELCOME_MSG}
    Run Keyword If    'True'=='${status}'    Fail    "Error: PW Field Accepted Value Without Small Character"

    #Verify Fourth PW Regex Criteria by Entering PW Without Special Character
    Scroll Element Into View        ${PASSWORD_FEILD}
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${WRONG_PW_REGEX4}  clear=True
    Set Selenium Implicit Wait  5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${WRONG_PW_REGEX4}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    ${status}=    Run Keyword And Return Status    Page Should Contain Element    ${WELCOME_MSG}
    Run Keyword If    'True'=='${status}'    Fail    "Error: PW Field Accepted Value Without Special Character"

Verify First Name Field Regex
    #Will Enter Valid Inputs For All Field Except First Name Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${WRONG_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${CORRECT_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${CORRECT_MOBNUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Page Should Contain Element     ${WRONG_FORMAT_ALERT}    "Error: Form Accepted First Name With Small Charachter"

Verify Last Name Field Regex
    #Will Enter Valid Inputs For All Field Except Last Name Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${CORRECT_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${WRONG_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${CORRECT_MOBNUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Page Should Contain Element     ${WRONG_FORMAT_ALERT}    "Error: Form Accepted Last Name With Small Charachter"

Verify Email Field Regex
    #Will Enter Valid Inputs For All Field Except Email Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${CORRECT_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${CORRECT_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${CORRECT_MOBNUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${WRONG_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Page Should Contain Element     ${WRONG_FORMAT_ALERT}    "Error: Form Accepted Wrog Regex For Email Field"

Verify Mobile NUM Field Regex
    #Will Enter Valid Inputs For All Field Except Mobile Num Field to Verify its Regex
    Sleep   5s
    Scroll Element Into View        ${FIRST_NAME_FEILD}
    Click Element    ${FIRST_NAME_FEILD}
    Input Text  ${FIRST_NAME_FEILD}  ${CORRECT_FNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${LAST_NAME_FEILD}
    Input Text  ${LAST_NAME_FEILD}  ${CORRECT_LNAME_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${MOB_NUM_FEILD}
    Click Element    ${MOB_NUM_FEILD}
    Input Text  ${MOB_NUM_FEILD}  ${WRONG_MOB_NUM_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Scroll Element Into View        ${EMAIL_FEILD}
    Click Element    ${EMAIL_FEILD}
    Input Text  ${EMAIL_FEILD}  ${CORRECT_EMAIL_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${PASSWORD_FEILD}
    Input Text  ${PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    Sleep   5s
    Click Element    ${CONFIRM_PASSWORD_FEILD}
    Input Text  ${CONFIRM_PASSWORD_FEILD}  ${CORRECT_PW_REGEX}  clear=True
    Set Selenium Implicit Wait  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Click Element    ${CONFIRM_BTN}
    Sleep  5s
    CapturePageScreenshot   ../Results/Ato/SignUpForm_{index}.png
    Page Should Contain Element     ${WRONG_FORMAT_ALERT}    "Error: Form Accepted Wrong Regex For Mobile Number Field"