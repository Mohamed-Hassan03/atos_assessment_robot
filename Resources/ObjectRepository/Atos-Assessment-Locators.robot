*** Variables ***
${TRAVEL_PAGE_URL}=          https://www.phptravels.net/home
${SIGN_UP_DDL} =             xpath:(//*[@id="dropdownCurrency"])[2]
${SIGN_UP_ICON}=             xpath://*[@class="dropdown-item tr"]

${FIRST_NAME_FEILD}=         xpath://*[@id="headersignupform"]//*[@name="firstname"]
${LAST_NAME_FEILD}=          xpath://*[@id="headersignupform"]//*[@name="lastname"]
${MOB_NUM_FEILD}=            xpath://*[@id="headersignupform"]//*[@name="phone"]
${EMAIL_FEILD}=              xpath://*[@id="headersignupform"]//*[@name="email"]
${PASSWORD_FEILD}=           xpath://*[@id="headersignupform"]//*[@name="password"]
${CONFIRM_PASSWORD_FEILD}=   xpath://*[@id="headersignupform"]//*[@name="confirmpassword"]
${CONFIRM_BTN}=              xpath://*[@class="signupbtn btn_full btn btn-success btn-block btn-lg"]

${WELCOME_MSG}=              xpath://*[@class="text-align-left"]

${LOGIN_USERNAME}=           xpath://*[@id="loginfrm"]//*[@name="username"]
${LOGIN_PW}=                 xpath://*[@id="loginfrm"]//*[@name="password"]
${LOGIN_BTN}=                xpath://*[@class="btn btn-primary btn-lg btn-block loginbtn"]

${WRONG_FORMAT_ALERT}=       xpath://*[@class="alert alert-danger"]
*** Keywords ***
