*** Settings ***
Documentation  Atos-Assessment01
Library  SeleniumLibrary
Library  RequestsLibrary
Library  JSONLibrary
Library  Collections
Library  os
Library  String
Resource  ../../../Resources/Web/Atos-Assessment-FlowKeywords.robot
Resource  ../../../Resources/Web/PO/Atos-Assessment-PO.robot
#Resource  ../../../Resources/TestData/Atos-Assessment/Atos-Assessment-TestData.robot
Library    OperatingSystem
Library    Collections
#Suite Setup     Atos-Assessment-FlowKeywords.Suite Setup
#Suite Teardown  Atos-Assessment-FlowKeywords.End All Test

*** Keywords ***
Set Global Data
    [Arguments]  ${TC_ID}
    @{TestData} =  Set Variable  ${${TC_ID}_TD}
    Set Global Variable  ${AUTOMATIONID}  ${TestData[0]}
    Set Global Variable  ${USERNAME}      ${TestData[1]}
    Set Global Variable  ${PASSWORD}      ${TestData[2]}

*** Test Cases ***

#Robot  -d results --variable BROWSER:chrome Tests/Atos-Assessment/Atos-Assessment01.robot

##======================================================================
##=========================New Flows====================================
GC01 : Navigate to Sign UP Page, Enter Valid Entries and Verify Login
    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
    Atos-Assessment-FlowKeywords.Navigate to Travels Page
    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
    Atos-Assessment-FlowKeywords.Verify success Registration and Login
    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test

#
#GC02 : Navigate to Sign UP Page and Verify PW Regex
#    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
#    Atos-Assessment-FlowKeywords.Navigate to Travels Page
#    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
#    Atos-Assessment-FlowKeywords.Verify Password Feild Regex
#    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test
#
#GC03 : Navigate to Sign UP Page and Verify First Name Field Regex
#    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
#    Atos-Assessment-FlowKeywords.Navigate to Travels Page
#    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
#    Atos-Assessment-FlowKeywords.Verify First Name Field Regex
#    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test
#
#GC04 : Navigate to Sign UP Page and Verify Last Name Field Regex
#    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
#    Atos-Assessment-FlowKeywords.Navigate to Travels Page
#    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
#    Atos-Assessment-FlowKeywords.Verify Last Name Field Regex
#    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test
#
#GC05 : Navigate to Sign UP Page and Verify Email Field Regex
#    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
#    Atos-Assessment-FlowKeywords.Navigate to Travels Page
#    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
#    Atos-Assessment-FlowKeywords.Verify Email Field Regex
#    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test
#
#
#GC06 : Navigate to Sign UP Page and Verify Mobile Number Field Regex
#    [Setup]     Atos-Assessment-FlowKeywords.Begin Web Test
#    Atos-Assessment-FlowKeywords.Navigate to Travels Page
#    Atos-Assessment-FlowKeywords.Navigate to Sign Up Page
#    Atos-Assessment-FlowKeywords.Verify Mobile NUM Field Regex
#    [Teardown]  Atos-Assessment-FlowKeywords.End Web Test